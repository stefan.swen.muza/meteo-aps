<html>
<!--UPUTE ZA KORIŠTENJE:
    Prije prvog korištenja treba prilagoditi varijable u dijelu "varijable koje se prilagođavaju postaji".
    Većinu varijabli treba prepisati iz ditalnog dnevnika motrenja, a one koje se ne nalaze u digitalnom dnevniku motritelj upisuje vezano uz tip uređaja i mjerne jedinice koje se koriste u radu.
    Ostatak koda NE MJENJATI!
    Program radi u interent pregledniku. Preporuka je na uređaju na kojem će se koristiti program uvjek imati najnoviju verziju internet preglednika.

    O PROGRAMU:
    Verzija 2.1.1 računa temperaturu rostišta, stvarni tlak vodene pare, relativnu vlagu zraka te svodi tlak na 0°C i na srednju morsku razinu (u planu je dodavanje mogućnosti da umjesto svođenja na SMR program računa visinu izobarne plohe).
    Aplikacija također izračunava synop kodove te šifrira prvi odjeljak synop izvještaja, temperaturu mora u drugom odjeljku te 1., 2., 3. ,4. i 7. grupu trećeg odjeljka i šifrira grupu za novi snijeg te u izvještaj uključuje i specijalne grupe koje motritelj sam unosi.

    ŠTO JE NOVO (cijelu povijest verzija pogledati na dnu koda):
    Program od sad izbacuje upozorenje ako je za visinu novog snijega upisano više od 99 cm.
    Manje vizualne promjene.

    IZJAVA O ODRICANJU OD ODGOVORNOSTI:
    Ovaj program je napisan kao pomoćno sredstvo za dobivanje izračunatih vrijednosti i synop izvještaja u slučaju kvara računalne opreme ili nestanka struje na meteorološkoj postaji.
    Program trebaju koristiti školovani motritelji koji znaju kako kako trebaju izgledati dobiveni podaci i synop izvještaj kako bi mogli ispraviti dobiveni podatak u slučaju greške u programu.
    Tvorac programa ne preuzima odgovornost za eventualnu štetu nastalu korištenjem ovog programa.
    
    KONTAKT:
    Pitanja, prijedlozi, kritike, uočeni problemi i sve ostalo vezano uz rad programa se može slati na e-mail adresu: stefan.muza@cirus.dhz.hr-->
<head>
<script>
// varijable koje se prilagođavaju postaji (mjenjati u skladu za uputama u komentarima za svaku varijablu)
var II = 14; //broj bloka
var iii = 246; //broj postaje
var h = 167; //nadmorska visina postaje [m]
var hb = 167.93; //nadmorska visina barometra [m]
var hsr = 290; //srednja nadmorska visina u krugu radiusa 150 km od postaje [m]
var GS = 46.3; //geografska širina postaje [0.1°]
var pk = 100; //postotak kopna u krugu radiusa 150 km
var db = 0; //dubina mora ispod postaje [m]
var dbs = 0; //srednja dubina mora ispod postaje u krugu od 150 km [m]
var ip = 0; //izobarna ploha (0 [SMR], 8 [850 hPa], 9 [925 hPa])
var bi1 = 1; //mjerne jedinice na barometru (1 [mmHg] ili 2 [hPa])
var bi2 = 2; //koriekcija po vrsti barometra (1, 2, 3, 4 ili 50)
var tmox = 2; //izvještavanje temperature mora (1 [da], 2 [ne])
var iW = 1; //jedinica brzine vjetra (0 - ocjenjeno [m/s], 1 - mjereno [m/s], 3 - ocjenjeno [kt], 4 mjereno [kt]
</script>
<meta charset="UTF-8" />
<title>SYNOP kalkulator | verzija: 2.1.1</title>
<style>
/* vizualni izgled */
table, th, td {
border: 1px solid black;
border-collapse: collapse;
}
td {
min-width: 70px;
}
.centar {
text-align: center;
}
.unos {
background-color: turquoise;
}
.unos1 {
background-color: blue;
color: white;
}
.izracun {
background-color: gray;
}
.prazno {
background-color: black;
}
.vrh {
background-color: yellow;
text-align: center;
}
.crveno {
color: red;
}
.gumb {
font-size: 15px;
background-color: gray;
color: black;
border: none;
padding: 10px 20px;
text-align: center;
text-decoration: none;
display: inline-block;
border-radius: 12px;
}
abbr {
position: relative;
}

abbr:hover::after {
position: absolute;
display: block;
padding: 1em;
background: seashell;
content: attr(title);
}
</style>
<script>
function load () {
var tww = document.getElementById("tw").value;
// varijable vremena
var d = new Date();
var day = d.getDate();
var day_displ = String('00' + day).slice(-2); // datum za ispis
var day_utc = d.getUTCDate(); // datum za synop
var month = d.getMonth() + 1;
var month_displ = String ('00' + month).slice(-2); // mjesec za ispis
var year = d.getFullYear(); // godina za ispis
var hour = d.getHours();
var hour_displ = String('00' + hour).slice(-2); // sat za ispis
var minutes = d.getMinutes();
var minutes_displ = String('00' + minutes).slice(-2); // minute za ispis
// uvjeti
if (minutes > 49) { // sat za synop
hour_utc = d.getUTCHours() + 1;
hour_utc1 = String('00' + hour_utc).slice(-2); 
} else {
hour_utc = d.getUTCHours();
hour_utc1 = String('00' + hour_utc).slice(-2);
}
if (hour_utc1 == "24") { //sat synop - nastavak
hour_utc_displ = 00
} else {
hour_utc_displ = hour_utc1
}
if (hour_utc_displ == "00") {
day_utc1 = day_utc + 1;
day_utc_displx = String('00' + day_utc1).slice(-2); 
} else {
day_utc1 = day_utc;
day_utc_displx = String('00' + day_utc1).slice(-2); 
}
if (bi1 == 1) {
unt = "mmHg"
} else {
unt = "hPa"
}
if (hour_utc_displ == "06") {
Re = " (18 UTC - 06 UTC)"
} else if (hour_utc_displ == "18") {
Re = " (06 UTC - 18 UTC)"
} else if (hour_utc_displ == "00") {
Re = " (18 UTC - 00 UTC)"
} else if (hour_utc_displ == "12") {
Re = " (06 UTC - 12 UTC)"
} else {
Re = " (Ne izvještava se)"
document.getElementById('oborina').setAttribute("style", "display:none");
}
if (hour_utc_displ == "06") {
ReSw = " (24 sata)"
} else if (hour_utc_displ == "18") {
ReSw = " (12 sati)"
} else {
ReSw = ''
}
document.getElementById("ReSw").innerHTML = ReSw;
if (hour_utc_displ !== "18") {
document.getElementById('max').setAttribute("style", "display:none");
} else {
document.getElementById('max').setAttribute("style", "display:block");
}
if (hour_utc_displ !== "06") {
document.getElementById('min').setAttribute("style", "display:none");
} else {
document.getElementById('min').setAttribute("style", "display:block");
}
if (hour_utc_displ !== "06") {
document.getElementById('min5').setAttribute("style", "display:none");
} else {
document.getElementById('min5').setAttribute("style", "display:block");
}
if (hour_utc_displ == "06") {
document.getElementById('sss').setAttribute("style", "display:block");
} else if (hour_utc_displ == "18") {
document.getElementById('sss').setAttribute("style", "display:block");
} else {
document.getElementById('sss').setAttribute("style", "display:none");
}
if (hour_utc_displ == "06") {
document.getElementById('r24').setAttribute("style", "display:block");
} else if (hour_utc_displ == "12") {
document.getElementById('r24').setAttribute("style", "display:block");
} else {
document.getElementById('r24').setAttribute("style", "display:none");
}
if (tmox == "1") {
document.getElementById('twd').setAttribute("style", "display:block");
} else {
document.getElementById('twd').setAttribute("style", "display:none");
}
if (tmox == "2" || tww == '') {
document.getElementById("div").setAttribute("style", "display:none");
} else {
document.getElementById("div").setAttribute("style", "display:block");
}
if (ip == "0") {
bxxx = "P"
} else {
bxxx = "H"
}

//zaglavlje programa i nulti odjeljak
time_date = "POSTAJA: " + II + iii + " | NADMORSKA VISINA: " + h + " m | DATUM: " + day_displ + "." + month_displ + "." + year + ". | VRIJEME: " + hour_displ + ":" + minutes_displ + " | TERMIN: " + hour_utc_displ + " UTC"
var odjeljak0 = "AAXX" + " " + day_utc_displx + hour_utc_displ + iW + " " + II + iii;
document.getElementById("timedate").innerHTML = time_date;
document.getElementById("bxxx").innerHTML = bxxx;
document.getElementById("Re").innerHTML = Re;
document.getElementById("odjeljak0").innerHTML = odjeljak0;
return odjeljak0;
}

//vidljivost

function vidljivost () {
var VVX = document.getElementById("km").value; //vidljivost
if (VVX == "") {
alert("Potrebno upisati vidljivost!");
VVX1 = "//";
} else {
if (VVX < 0.1) {
VV = 00;
} else if (VVX == 0.1) {
VV = 01;
} else if (VVX == 0.2) {
VV = 02;
} else if (VVX == 0.3) {
VV = 03;
} else if (VVX == 0.4) {
VV = 04;
} else if (VVX == 0.5) {
VV = 05;
} else if (VVX == 0.6) {
VV = 06;
} else if (VVX == 0.7) {
VV = 07;
} else if (VVX == 0.8) {
VV = 08;
} else if (VVX == 0.9) {
VV = 09;
} else if (VVX == 1) {
VV = 10;
} else if (VVX == 1.1) {
VV = 11;
} else if (VVX == 1.2) {
VV = 12;
} else if (VVX == 1.3) {
VV = 13;
} else if (VVX == 1.4) {
VV = 14;
} else if (VVX == 1.5) {
VV = 15;
} else if (VVX == 1.6) {
VV = 16;
} else if (VVX == 1.7) {
VV = 17;
} else if (VVX == 1.8) {
VV = 18;
} else if (VVX == 1.9) {
VV = 19;
} else if (VVX == 2) {
VV = 20;
} else if (VVX == 2.1) {
VV = 21;
} else if (VVX == 2.2) {
VV = 22;
} else if (VVX == 2.3) {
VV = 23;
} else if (VVX == 2.4) {
VV = 24;
} else if (VVX == 2.5) {
VV = 25;
} else if (VVX == 2.6) {
VV = 26;
} else if (VVX == 2.7) {
VV = 27;
} else if (VVX == 2.8) {
VV = 28;
} else if (VVX == 2.9) {
VV = 29;
} else if (VVX == 3) {
VV = 30;
} else if (VVX == 3.1) {
VV = 31;
} else if (VVX == 3.2) {
VV = 32;
} else if (VVX == 3.3) {
VV = 33;
} else if (VVX == 3.4) {
VV = 34;
} else if (VVX == 3.5) {
VV = 35;
} else if (VVX == 3.6) {
VV = 36;
} else if (VVX == 3.7) {
VV = 37;
} else if (VVX == 3.8) {
VV = 38;
} else if (VVX == 3.9) {
VV = 39;
} else if (VVX == 4) {
VV = 40;
} else if (VVX == 4.1) {
VV = 41;
} else if (VVX == 4.2) {
VV = 42;
} else if (VVX == 4.3) {
VV = 43;
} else if (VVX == 4.4) {
VV = 44;
} else if (VVX == 4.5) {
VV = 45;
} else if (VVX == 4.6) {
VV = 46;
} else if (VVX == 4.7) {
VV = 47;
} else if (VVX == 4.8) {
VV = 48;
} else if (VVX == 4.9) {
VV = 49;
} else if (VVX < 5.5) {
VV = 50;
} else if (VVX < 6.5) {
VV = 56;
} else if (VVX < 7.5) {
VV = 57;
} else if (VVX < 8.5) {
VV = 58;
} else if (VVX < 9.5) {
VV = 59;
} else if (VVX < 10.5) {
VV = 60;
} else if (VVX < 11.5) {
VV = 61;
} else if (VVX < 12.5) {
VV = 62;
} else if (VVX < 13.5) {
VV = 63;
} else if (VVX < 14.5) {
VV = 64;
} else if (VVX < 15.5) {
VV = 65;
} else if (VVX < 16.5) {
VV = 66;
} else if (VVX < 17.5) {
VV = 67;
} else if (VVX < 18.5) {
VV = 68;
} else if (VVX < 19.5) {
VV = 69;
} else if (VVX < 20.5) {
VV = 70;
} else if (VVX < 21.5) {
VV = 71;
} else if (VVX < 22.5) {
VV = 72;
} else if (VVX < 23.5) {
VV = 73;
} else if (VVX < 24.5) {
VV = 74;
} else if (VVX < 25.5) {
VV = 75;
} else if (VVX < 26.5) {
VV = 76;
} else if (VVX < 27.5) {
VV = 77;
} else if (VVX < 28.5) {
VV = 78;
} else if (VVX < 29.5) {
VV = 79;
} else if (VVX < 35) {
VV = 80;
} else if (VVX < 38) {
VV = 81;
} else if (VVX < 43) {
VV = 82;
} else if (VVX < 48) {
VV = 83;
} else if (VVX < 53) {
VV = 84;
} else if (VVX < 58) {
VV = 85;
} else if (VVX < 63) {
VV = 86;
} else if (VVX < 68) {
VV = 87;
} else if (VVX <= 70) {
VV = 88;
} else {
VV = 89;
}
var VVs = String(('00' + VV).slice(-2));

if (VVX == '') {
VVsq = "//";
} else {
VVsq = VVs;
}
document.getElementById("VVizr").innerHTML = VVsq;
return VVsq;
}
}

// ukupna naoblaka

function naoblaka () {
var NN10 = document.getElementById("nn").value; // naoblaka X/10
if (NN10 == '' || NN10 > 10) {
alert("Vrijednost naoblake treba biti od 0 do 10!");
} else {
var NN8 = (NN10 / 10 * 8).toFixed(0);
return NN8;
}
}

// visoki oblaci

function visokiobl () {
var ch = document.getElementById("ch").value // šifra visokih oblaka
if (ch == '') {
alert("Potrebno upisati vrijednost od 0 do 9 ili /")
} else {
chh = ch
}
}

// srednji oblaci

function srednjiobl () {
var cm = document.getElementById("cm").value // šifra srednjih oblaka
if (cm == '') {
alert("Potrebno upisati vrijednost od 0 do 9 ili /");
} else {
cmm = cm;
}
}

function srednjiosmine () {
var NNm10 = document.getElementById("nnm").value;
if (NNm10 == '') {
alert("Potrebno je upisati količinu srednjih oblaka u desetinama ili / ako oblaci nisu vidljivi!");
} else {
NNm8 = (NNm10 / 10 * 8).toFixed(0);
}
return NNm8;
}

function srednjioblvis () {
var kmm = document.getElementById("kmm").value; //visina podnice najnižeg oblaka [km]
if (kmm < 0.05){
hm = 0;
} else if (kmm < 0.1) {
hm = 1;
} else if (kmm < 0.2) {
hm = 2;
} else if (kmm < 0.3) {
hm = 3;
} else if (kmm < 0.6) {
hm = 4;
} else if (kmm < 1) {
hm = 5;
} else if (kmm < 1.5) {
hm = 6;
} else if (kmm < 2) {
hm = 7;
} else if (kmm < 2.5) {
hm = 8;
} else if (kmm >= 2.5) {
hm = 9;
} else {
hm = "/";
} 

if (kmm == '') {
hmq = ''
hmq1 = 9;
} else {
hmq = hm;
hmq1 = hm;
}
document.getElementById("hmizr").innerHTML = hmq;
return hmq1;
}

// niski oblaci

function niskiobl () {
var cl = document.getElementById("cl").value // šifra niskih oblaka
if (cl == '') {
alert("Potrebno upisati vrijednost od 0 do 9 ili /");
} else {
cll = cl;
}
}

function niskiosmine () {
var NNl10 = document.getElementById("nnl").value;
if (NNl10 == '') {
alert("Potrebno je upisati količinu niskih oblaka u desetinama ili / ako oblaci nisu vidljivi!");
} else {
NNl8 = (NNl10 / 10 * 8).toFixed(0);
}
return NNl8;
}

function niskioblvis () {
var kml = document.getElementById("kml").value; //visina podnice najnižeg oblaka [km]
if (kml < 0.05){
hl = 0;
} else if (kml < 0.1) {
hl = 1;
} else if (kml < 0.2) {
hl = 2;
} else if (kml < 0.3) {
hl = 3;
} else if (kml < 0.6) {
hl = 4;
} else if (kml < 1) {
hl = 5;
} else if (kml < 1.5) {
hl = 6;
} else if (kml < 2) {
hl = 7;
} else if (kml < 2.5) {
hl = 8;
} else if (kml >= 2.5) {
hl = 9;
} else {
hl = "/";
}

if (kml == '') {
hlq = ''
hlq1 = 0;
} else {
hlq = hl;
hlq1 = hl;
}
document.getElementById("hlizr").innerHTML = hlq;
return hlq1;
}

// visina najnižih oblaka

function najniziobl () {
var cl = document.getElementById("cl").value; // šifra niskih oblaka
if (cl == "/") {
hnis = "/"
} else {
hnis = niskioblvis ();
}
var hsred = srednjioblvis ();
if (hsred < hnis) {
alert("Visina srednjih oblaka manja od visine niskih oblaka!")
} else {
if (hnis == "0") {
hobl = hsred;
} else if (hnis == "/") {
hobl = "/"
} else {
hobl = Math.min(hsred, hnis)
}
}
return hobl;
}

// vjetar

function vjetarsmjer () { //smjer vjetra
var dd = document.getElementById("dd").value;
var DD = document.getElementById("DD").value;
if (dd == '' && DD == '') {
alert("Mora biti upisan podatak ili mjerenog ili opažanog smjera vjetra!");
} else {
if ( dd == '') {
ddx = (DD / 32 * 36).toFixed(0);
ddxd = String(('00' + ddx).slice(-2));
} else {
ddx = dd;
ddxd = String(('00' + ddx).slice(-2));
}
}
return ddxd;
}

function vjetarbrzina () { //brzina vjetra
var ff = document.getElementById("ff").value;
var FF = document.getElementById("FF").value;
if (ff == '' && FF == '') {
alert("Mora biti unesen ili podatak o brzini vjetra ili podatak o jačini vjetra!");
} else { 
if (ff == '') {
if (FF == 0) {
ffxoi = 0
} else if (FF == "1") {
ffxoi = 1
} else if (FF == "2") {
ffxoi = 2
} else if (FF == "3") {
ffxoi = 4
} else if (FF == "4") {
ffxoi = 6
} else if (FF == "5") {
ffxoi = 9
} else if (FF == "6") {
ffxoi = 12
} else if (FF == "7") {
ffxoi = 16
} else if (FF == "8") {
ffxoi = 19
} else if (FF == "9") {
ffxoi = 23
} else if (FF == "10") {
ffxoi = 27
} else if (FF == "11") {
ffxoi = 31
} else if (FF == "12") {
ffxoi = 35
} else {
ffxoi = "/"
}
} else {
ffxoi = ff;
}
}
ffxoix = String(('00' + ffxoi).slice(-2));    
return ffxoix;
}

// vlaga i tlak

function vlagatlak () {
    
var TS = document.getElementById("suhatemp").value;
var TM = document.getElementById("mokratemp").value;
var Tb = document.getElementById("barotemp").value;
var b = document.getElementById("baroočitanje").value;
// cijepanje mokre temperature
TMs = TM.slice(0, TM.search(/\d/)); // slovo
TMb = TM.replace(TMs, ''); // broj
if (TMs == "L"){
TMs1 = "L"
} else if (TMs == "l") {
TMs1 = "L"
} else if (TMs == "V") {
TMs1 = "V"
} else if (TMs == "v") {
TMs1 = "V"
} else {
TMs1 ="+"
}
// decimalna točka
var TT = TS / 10;
if (TMs1 == "V") {
HT = TMb / -10
} else if (TMs1 == "L") {
HT = TMb / -10
}
else {
HT = TMb /10
}
var TTB = Tb / 10; // temperatura na barometru
var BBB = b / 10; // očitanje barometra
//konstante
//pozitivna temperatura
p1 = 6.10780;
p2 = 17.08085;
p3 = 234.175;
//negativna temperatura - voda
pv1 = 6.10780;
pv2 = 17.84362;
pv3 = 245.425;
//negativna temperatura - led
pl1 = 6.10714;
pl2 = 22.44294;
pl3 = 272.440;
//psihrometrijska konstanta
pp1 = 0.000660 * (1 + 0.00115 * HT );
pp2 = 0.000582;

//koje konstante koristiti
if (TMs1 == "V") {
c = pp1
c1 = pv1
c2 = pv2
c3 = pv3
} else if (TMs1 == "L") {
c = pp2
c1 = pl1
c2 = pl2
c3 = pl3
}
else {
c = pp1
c1 = p1
c2 = p2
c3 = p3
}

//druge konstante
e = 2.718282;
if (bi1 == 1) {
kortl = 1.333224
}
else if (bi1 == 2) {
kortl = 1
}
else {
kortl = 0
}
var cb1 = 0.000163;
if (bi2 == 1) {
cb2 = 0.0
} else if (bi2 == 2) {
cb2 = 33.09
}  else if (bi2 == 3) {
cb2 = 45.33
}  else if (bi2 == 4) {
cb2 = 15.30
} else {
cb2 = 49.46
}

// tlak

var ALFA = pk * 0.01;
var KOS = Math.cos(2.0 * GS * 3.141593/180.0);
var GF0 = 9.80616 * (1.-(0.0026373 * KOS)) + (0.0000059 * (Math.pow(KOS,2)));
var GFHB = GF0 - 0.000003086 * hb + 0.000001118 * ALFA * (hb - hsr)- 0.00000688 * (1-ALFA) * (db - dbs);
var P0 = BBB * kortl
var P0_1 = P0 - (cb1 * (P0 + cb2) * TTB);
var P0_final = P0_1 + P0 * (GFHB/9.80665 - 1.0);
var P0_final1 = (P0_final).toFixed(1);
var P0_final2 = P0_final1 * 10;
if (P0_final1 < 1000) {
P0P0P0P0 = String(P0_final1 * 10);
} else {
P0P0 = ((P0_final1 - 1000) * 10).toFixed(0);
P0P0P0P0 = String('0000' + P0P0).slice(-4);
}
document.getElementById("P0P0P0P0").innerHTML = P0P0P0P0; // tlak 0°C

var T_SMR = TT + (0.5 * hb / 100);
var T_S = (TT + T_SMR) / 2;
var hg = hb * 9.81;
var RT = 286.9 * (T_S + 273.15);
var hgRT = hg / RT;
var k = Math.pow(e, hgRT) - 1;
var dP = P0_final * k;
var P_hPa = P0_final + dP;
var P_hPaf = P_hPa.toFixed(1)
if (P_hPa < 1000) {
PPPP = (P_hPa * 10).toFixed(0);
} else {
PP = ((P_hPa - 1000) * 10).toFixed(0);
PPPP = String('0000' + PP).slice(-4);
}
document.getElementById("PPPP").innerHTML = PPPP; // tlak SMR
//vlaga
mi1 = c2 * HT / (c3 + HT); 
mi2 = c2 * TT / (c3 + TT);
SVPHT = c1 * Math.pow(e, mi1); //tlak zasićene vodene pare za mokru temperaturu
SVPTT = c1 * Math.pow(e, mi2); //tlak zasićene vodene pare za suhu temperaturu
VPp = SVPHT - c * P0_final * (TT - HT); //stvarni tlak vodene pare (e)
VP = VPp.toFixed(1);
VPD = VP * 10;
//temperatura rosišta prije mora proći provjeru je li negativna ili pozitivna i onda se određuje temperatura rosišta
TDp = p3 * Math.log(VPp / p1) / (p2 - Math.log(VPp / p1));
TDn = pv3 * Math.log(VPp / pv1) / (pv2 - Math.log(VPp / pv1));
if (TDp >= 0.0) {
TDr = TDp
} else {
TDr = TDn
}
TD = TDr.toFixed(1); //temperatura rosište (td)
TDD = TD * 10;
mi3 = c2 * TDr / (c3 + TDr);
SVPTD = c1 * Math.pow(e, mi3);
UU = 100 * SVPTD / SVPTT;
U = UU.toFixed(0); //vlaga zraka (U)
document.getElementById("tdd").innerHTML = TDD;
document.getElementById("vpd").innerHTML = VPD;
document.getElementById("uuu").innerHTML = U;
if (U > 100) {
alert ("Mokra temperatura ne može biti veća od suhe!");
} else {
return {
P0_final2,
P0P0P0P0,
PPPP,
TDD
};
}
}

//promjena tlaka

function tlakprom () {
var proms = document.getElementById("tlakp").value;
var promsx = parseInt(proms);
let vtprom = vlagatlak ();
let tlakpred = vtprom.P0_final2;
if (proms < 7000) {
prom1 = promsx + 10000;
} else {
prom1 = promsx;
}
if (tlakpred < 7000) {
tlakpred1 = tlakpred + 10000;
} else {
tlakpred1 = tlakpred;
}
tlakpr = Math.abs(prom1 - tlakpred1);
tlakpr1 = String('000' + tlakpr).slice(-3);
if (proms == '') {
tlakpr2 = "///"
} else {
tlakpr2 = tlakpr1
}
document.getElementById("prom").innerHTML = tlakpr2;
return tlakpr2;
}

function approvjera () {
var approv = document.getElementById("appp").value;
if (approv == ''){
alert("Upisati šifru tendencije tlaka zraka!");
} else {
approv1 = approv;
}
}

// oborina terminska

function oborina () {
var R = document.getElementById("R").value;
if (R == '') {
Rs = 000;
} else if (R == "0") {
Rs = 990;
} else if (R == "1") {
Rs = 991;
} else if (R == "2") {
Rs = 992;
} else if (R == "3") {
Rs = 993;
} else if (R == "4") {
Rs = 994;
} else if (R == "5") {
Rs = 995;
} else if (R == "6") {
Rs = 996;
} else if (R == "7") {
Rs = 997;
} else if (R == "8") {
Rs = 998;
} else if (R == "9") {
Rs = 999;
} else {
Rs = (R / 10).toFixed(0)
}
Rs1 = String('000' + Rs).slice(-3);
if (hour_utc_displ == "06" || hour_utc_displ == "18") {
iR = 1;
tR = 2;
} else if (hour_utc_displ == "00" || hour_utc_displ == "12") {
iR = 1;
tR = 1;
} else {
iR = 4;
tR = 0;
}
return {
Rs1,
iR,
tR
};
}

// izračun synop izvještaja

function snijegnew_test (){
var snijegnew25 = document.getElementById("ssnov").value; // novi snijeg
snijeg25 = parseInt(snijegnew25);
if (snijeg25 > 99){
alert("Visina novog snijega mora biti manja od 100 cm!")
} else {
ssnov_bla = snijegnew25;
}
}

function synop () {
var TSsyn = document.getElementById("suhatemp").value; // temperatura zraka
var ap = document.getElementById("appp").value; // tendencija tlaka
var poj = document.getElementById("wwW1W2").value; // pojave
var chs = document.getElementById("ch").value; // visoki oblaci
var cms = document.getElementById("cm").value; // srednji oblaci
var cls = document.getElementById("cl").value; // niski oblaci
var nnms = document.getElementById("nnm").value; // srednja naoblaka
var nnls = document.getElementById("nnl").value; // niska naoblaka
var tws = document.getElementById("tw").value; // temperatura vode
var txs = document.getElementById("tx").value; // maksimalna temperatura
var tns = document.getElementById("tn").value; // minimalna temperatura
var ess = document.getElementById("es").value; // stanje tla
var ems = document.getElementById("em").value; // stanje tla sa snijegom
var tn5s = document.getElementById("tn5").value; // minimalna temperatura na 4 cm iznad tla
var r24s = document.getElementById("R24").value; // oborina 24 sata
var r24s1 = String('0000' + r24s).slice(-4);
var snijegnov = document.getElementById("sssss").value; // količina snijega
var snijegnew = document.getElementById("ssnov").value; // novi snijeg
var spsp = document.getElementById("spspspsp").value; // posebne grupe
var vid = vidljivost ();
var NN8x = naoblaka ();
var hob = najniziobl ();
var ddxds = vjetarsmjer ();
var ffxfs = vjetarbrzina ();
let vt = vlagatlak ();
let PP = vt.PPPP;
P0P01 = vt.P0P0P0P0;
TD1 = vt.TDD;
if (TD1 < 0) {
sntd = 1;
TD2 = TD1 * -1;
} else {
sntd = 0
TD2 = TD1;
}
TD3 = String('000' + TD2).slice(-3);
if (TSsyn < 0) {
sntt = 1;
TSsyn1 = TSsyn * -1;
} else {
sntt = 0;
TSsyn1 = TSsyn;
}
TSsyn2 = String('000' + TSsyn1).slice(-3);
tpr = tlakprom ();
if (poj == ''){
IX = 2;
grupa7 = '';
} else {
IX = 1;
grupa7 = " 7" + poj;
}
if (nnls == "0") {
nnlsx10 = nnms;
} else {
nnlsx10 = nnls;
}
nnlsx = (nnlsx10 / 10 * 8).toFixed(0);
if (chs == "0" && cms == "0" && cls == "0") {
grupa8 = '';
} else {
grupa8 = " 8" + nnlsx + cls + cms + chs;
}
let obor = oborina ();
let iRs = String(obor.iR);
Rs1s = obor.Rs1;
tRs = obor.tR;
if (iRs == "4") {
grupa6 = ''
} else {
grupa6 = " 6" + Rs1s + tRs;
}
if (tws < 0) {
snw = 1;
tws1 = tws * -1;
} else {
snw = 0;
tws1 = tws;
}
tws2 = String('000' + tws1).slice(-3);
if (tmox == "2" || tws == '') {
document.getElementById("div").setAttribute("style", "display:none");
} else {
document.getElementById("div").setAttribute("style", "display:block");
}

// maksimalna temperatura
if (txs < 0) {
sntx = 1;
txs1 = txs * -1;
} else {
sntx = 0;
txs1 = txs;
}
txs2 = String('000' + txs1).slice(-3);
if (txs == '') {
grupa31 = ''
} else {
grupa31 = " 1" + sntx + txs2;
}

// minimalna temperatura
if (tns < 0) {
sntn = 1;
tns1 = tns * -1;
} else {
sntn = 0;
tns1 = tns;
}
tns2 = String('000' + tns1).slice(-3);
if (tns == '') {
grupa32 = ''
} else {
grupa32 = " 2" + sntn + tns2;
}

// minimalna temperatura na 5 cm iznad tla

if (tn5s < 0) {
tn5s1 = tn5s / -10;
} else {
tn5s1 = tn5s / 10;
}
tn5s2 = (tn5s1).toFixed(0);
if (tn5s == '') {
tn5s3 = "//";
} else {
tn5s3 = String('00' + tn5s2).slice(-2);
}
if (tn5s == ''){
sntn5 = "/"
} else if (tn5s < 0){
sntn5 = 1;
} else {
sntn5 = 0;
}
if (ess == '') {
ess1 = "/"
} else {
ess1 = String(ess);
}
if (hour_utc_displ == "06") {
grupa33 = " 3" + ess1 + sntn5 + tn5s3;
} else {
grupa33 = '';
}

// snijeg

if (ems == '') {
ems1 = "/";
} else {
ems1 = ems;
}

snijegnov1 = String('000' + snijegnov).slice(-3);

if (hour_utc_displ == "06" || hour_utc_displ == "18") {
if (ems1 == "0") {
grupa34 = " 4" + ems1 + "///";
} else if (ems1 == '/') {
grupa34 = '';
} else if (ems1 == "1" || ems1 == "5") {
grupa34 = " 4" + ems1 + "998"
} else if (snijegnov1 < 0.5) {
grupa34 = " 4" + ems1 + "997"
} else {
grupa34 = " 4" + ems1 + snijegnov1;
}
} else {
grupa34 = '';
}

//novi snijeg

snijegnew1 = String('00' + snijegnew).slice(-2);

if (hour_utc_displ == "06" || hour_utc_displ == "18") {
if (snijegnew == '') {
grupa931 = ''
} else if (snijegnew < 0.1) {
grupa931 = " 93197"
} else if (snijegnew == "0.1") {
grupa931 = " 93191"
} else if (snijegnew == "0.2") {
grupa931 = " 93192"
} else if (snijegnew == "0.3") {
grupa931 = " 93193"
} else if (snijegnew == "0.4") {
grupa931 = " 93194"
} else if (snijegnew == "0.5") {
grupa931 = " 93195"
} else if (snijegnew == "0.6") {
grupa931 = " 93196"
} else if (snijegnew > 400) {
grupa931 = " 93198"
} else {
grupa931 = " 931" + snijegnew1
}
} else {
grupa931 = ''
}


// oborina 24 sata
if (r24s == '' && hour_utc_displ == "06") {
grupa37 = " 70000";
} else if (r24s == '' && hour_utc_displ == "12"){
grupa37 = " 70000";
} else if (r24s == '') {
grupa37 = '';
} else if (r24s == "0") {
grupa37 = " 79999";
} else if (r24s > 9998) {
grupa37 = " 79998";
} else {
grupa37 = " 7" + r24s1;
}

// odjeljci
var odjeljak1 = iRs + IX + hobl + vid + " " + NN8x + ddxds + ffxfs + " 1" + sntt + TSsyn2 + " 2" + sntd + TD3 + " 3" + P0P01 + " 4" + PP + " 5" + ap + tpr + grupa6 + grupa7 + grupa8;
var odjeljak2 = "222//" + " 0" + snw + tws2;
if (grupa31== '' && grupa32 == '' && grupa33 == '' && grupa34 == '' && grupa37 == '' && grupa931 == '' && spsp == '') {
odjeljak3 = '';
} else {
odjeljak3 = "333" + grupa31 + grupa32 + grupa33 + grupa34 + grupa37 + grupa931 + " " + spsp;
}

// završni ispis
document.getElementById("odjeljak1").innerHTML = odjeljak1;
document.getElementById("odjeljak2").innerHTML = odjeljak2;
document.getElementById("odjeljak3").innerHTML = odjeljak3;
}
</script>
</head>
<body onload="load ()">
<table>
<tr>
<td colspan="17" class="vrh"><b><p id="timedate"></p></b></td>
</tr>
<tr>
<td colspan="17" class="vrh"><b>TERMINSKE VRIJEDNOST</b></td>
</tr>
<tr class="centar">
<td class="unos"><abbr title="Vidljivost u kilometrima">km</abbr></td>
<td class="izracun">VV</td>
<td class="unos"><abbr title="Ukupna naoblaka">NN</abbr></td>
<td class="unos"><abbr title="Količina visokih oblaka">NN<sub>H</sub></abbr></td>
<td class="unos"><abbr title="Šifra visokih oblaka [0-9 ili /]">C<sub>H</sub></abbr></td>
<td class="unos"><abbr title="Količina srednjih oblaka">NN<sub>M</sub></abbr></td>
<td class="unos"><abbr title="Šifra srednjih oblaka [0-9 ili /]">C<sub>M</sub></abbr></td>
<td class="unos"><abbr title="Visina srednjih oblaka u kilometrima">km<sub>M</sub></abbr></td>
<td class="izracun">h<sub>M</sub></td>
<td class="unos"><abbr title="Količina niskih oblaka">NN<sub>L</sub></abbr></td>
<td class="unos"><abbr title="Šifra niskih oblaka [0-9 ili /]">C<sub>L</sub></abbr></td>
<td class="unos"><abbr title="Visina niskih oblaka u kilometrima">km<sub>L</sub></abbr></td>
<td class="izracun">h<sub>L</sub></td>
<td class="unos"><abbr title="Izmjereni smjer vjetra [00-36]">dd</abbr></td>
<td class="unos"><abbr title="Izmjerena brzina vjetra">ff</abbr></td>
<td class="unos"><abbr title="Opažani smjer vjetra [00-32]">DD</abbr></td>
<td class="unos"><abbr title="Jačina vjetra u Bf">FF</abbr></td>
</tr>
<tr class="centar">
<td class="unos"><input class="unos" type="text" size="3" id="km" onblur="vidljivost ()"></td>
<td class="izracun"><p id="VVizr"></p></td>
<td class="unos"><input class="unos" type="text" size="3" id="nn" onblur="naoblaka ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="nnh"></td>
<td class="unos"><input class="unos" type="text" size="3" id="ch" onblur="visokiobl ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="nnm" onblur="srednjiosmine ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="cm" onblur="srednjiobl ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="kmm" onblur="srednjioblvis (); najniziobl ()"></td>
<td class="izracun"><p id="hmizr"></p></td>
<td class="unos"><input class="unos" type="text" size="3" id="nnl" onblur="niskiosmine ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="cl" onblur="niskiobl ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="kml" onblur="niskioblvis (); najniziobl ()"></td>
<td class="izracun"><p id="hlizr"></td>
<td class="unos"><input class="unos" type="text" size="3" id="dd"></td>
<td class="unos"><input class="unos" type="text" size="3" id="ff"></td>
<td class="unos"><input class="unos" type="text" size="3" id="DD" onblur="vjetarsmjer ()"></td>
<td class="unos"><input class="unos" type="text" size="3" id="FF" onblur="vjetarbrzina ()"></td>
</tr>
<tr class="centar">
<td class="unos"><abbr title="Stanje tla [0-9]">E</abbr></td>
<td class="unos"><abbr title="Stanje tla sa snježnim pokrivačem [0-9]">E'</abbr></td>
<td class="unos"><abbr title="Temperatura zraka">T</abbr></td>
<td class="unos"><abbr title="Temperatura zraka po mokrom termometru">T'</abbr></td>
<td class="izracun">T<sub>d</sub></td>
<td class="izracun">e</td>
<td class="izracun">U</td>
<td class="unos"><abbr title="Temperatura na barometru">T<sub>b</abbr></sub></td>
<td class="unos"><abbr title="Očitanje barometra">b</abbr></td>
<td class="izracun">P<sub>0</sub></td>
<td class="izracun"><p id="bxxx"></p></td>
<td colspan="2" class="unos"><abbr title="Tlak na razni postaje prije 3 sata [hPa]">P<sub>0 (-3h)</sub></abbr></td>
<td colspan="2" class="unos"><abbr title="Tendencija tlaka zraka na razini postaje tijekom prethodnih 3 sata."><b>5appp</b></abbr></td>
<td colspan="2" class="unos"><abbr title="UTC 00, 06, 12, 18 - W1W2 za prethodnih 6 sati; UTC 03, 09, 15, 21 - W1W2 za prethodna 3 sata; svaki ostali termin - W1W2 za predhodni sat"><b>7wwW<sub>1</sub>W<sub>2</sub></b></abbr></td>
</tr>
<tr class="centar">
<td class="unos"><input class="unos" type="text" size="3" id="es"></td>
<td class="unos"><input class="unos" type="text" size="3" id="em"></td>
<td class="unos"><input class="unos" type="text" size="3" id="suhatemp"></td>
<td class="unos"><input class="unos" type="text" size="3" id="mokratemp"></td>
<td class="izracun"><p id="tdd"></p></td>
<td class="izracun"><p id="vpd"></p></td>
<td class="izracun"><p id="uuu"></p></td>
<td class="unos"><input class="unos" type="text" size="3" id="barotemp"></td>
<td class="unos"><input class="unos" type="text" size="3" id="baroočitanje" onblur="vlagatlak ()"></td>
<td class="izracun"><p id="P0P0P0P0"></p></td>
<td class="izracun"><p id="PPPP"></p></td>
<td colspan="2" class="unos"><input class="unos" type="text" size="3" id="tlakp" onblur="tlakprom ()"></td>
<td colspan="2" class="unos"><b>5</b><input class="unos" type="text" size="1" id="appp" onblur="approvjera ()"><b><p id="prom" style="display:inline">ppp</p></b></td>
<td colspan="2" class="unos"><b>7</b><input class="unos" type="text" size="4" id="wwW1W2"></td>
</tr>
<tr>
<td colspan="17" class="vrh"><b>OSTALE VRIJEDNOST</b></td>
</tr>
<tr>
<td colspan="17"><div id="twd"><abbr title="Temperatura mora">T<sub>w</sub></abbr> = <input class="unos" type="text" size="3" id="tw"></div>
<div id="max"><abbr title="Maksimalna temperatura zraka (06 UTC - 18 UTC)">T<sub>X</sub></abbr> = <input class="unos" type="text" size="3" id="tx"></div>
<div id="min"><abbr title="Minimalna temperatura zraka (18 UTC - 06 UTC)">T<sub>N</sub></abbr> = <input class="unos" type="text" size="3" id="tn"></div>
<div id="min5"><abbr title="Minimalna temperatura na 5cm iznad tla">T<sub>n5</sub></abbr> = <input class="unos" type="text" size="3" id="tn5"></div>
<div id="oborina"><abbr title="Količina oborine za navedeno razdoblje">R<p id="Re" style="display:inline"><p style="display:inline"></abbr> = <input class="unos" type="text" size="3" id="R"></p></div>
<div id="r24"><abbr title="Količina oborine tijekom prethodnih 24 sata">R<sub>24</sub></abbr> = <input class="unos" type="text" size="3" id="R24"></div>
<div id="sss">SNIJEG: <abbr title="Visina snijega">UKUPNI</abbr> = <input class="unos" type="text" size="3" id="sssss"> <abbr title="Visina novonapadalog snijega u navedenom razdoblju">NOVI</abbr><p id="ReSw" style="display:inline"><p style="display:inline"></abbr> = <input class="unos" type="text" size="3" id="ssnov" onblur="snijegnew_test ()"></div>
<abbr title="Specijalne grupe. Unosi se potreban broj puta u grupama po 5 znamenki, a grupe se međusobno razdvajaju razmakom.">S<sub>p</sub>S<sub>p</sub>S<sub>p</sub>S<sub>p</sub>S<sub>p</sub></abbr> = <input class="unos" type="text" size="40" id="spspspsp"><br>
</td>
</tr>
<tr>
<td class="centar" colspan="2"><button class="gumb" onclick="synop ()">Ispiši synop</button></td>
<td colspan="15"><div id="div0"><b><p class="crveno" id="odjeljak0" style="display:inline"></p></b></div><b><p class="synop" id="odjeljak1" style="display:inline"></p><br><div id="div"><p class="synop" id="odjeljak2" style="display:inline"></p></br></div><p class="synop" id="odjeljak3" style="display:inline"></p></b></td>
</tr>
</table>
UPUTE ZA KORIŠTENJE:<br>
Podaci se u polja unose <b>kao i u dnevnik motrenja glavnih meteoroloških postaja</b>. Program izračunate podatke ispisuje
u formatu u <b>kojem se prepisuju u dnevnik</b>.
Program izbacuje upozorenje ako nisu uneseni podaci neophodni za izradu synop izvještaja. Sva ostala polja za koja <i>ne postoje
podaci</i> se <b>ostavljaju prazna</b>.<br>
Dodatne upute za svako polje može se dobiti tako da se tapne na naziv polja.<br>
Program automatski pokazuje i skriva polja za unos podataka koji se šalju u synop izvještaju u određenim terminima. Prije svakog novog unosa podataka najbolje je osvježiti preglednik kako bi program prihvatio trenutno vrijeme i pokazao polja za unos za aktualni termin.<br>
<br>
Verzija: 2.1.1<br>
<br>
ŠTO JE NOVO:<br>
Program od sad izbacuje upozorenje ako je za visinu novog snijega upisano više od 99 cm.<br>
Manje vizualne promjene.
</body>
<!--POVIJEST VERZIJA:
    2.1.1 - Program od sad izbacuje upozorenje ako je za visinu novog snijega upisano više od 99 cm.
    Manje vizualne promjene.

    2.1.0 - Program od sada šifrira i novi snijeg.
    Manje vizualne promjene u svrhu integriranja opcije za upis novog snijega.
    Od sada pomoć pri upisu u određena polja se može dobiti tako da se tapne na naziv polja.
    Mogućnost promjene broja bloka prilikom editiranja u editoru teksta.

    2.0.3 - Ispravljeno računanje 3. grupe u 3. odjeljku.
    Program sada izbacuje ispravnu vrijednost grupe u slučaju da nije poznata minimalna temperatura na 5 cm iznad tla.

    2.0.2 - Ispravljeno računanje 4. grupe u 3. odjeljku.
    U slučaju da je stanje tla pod snijegom 1 ili 5, program od sada tu grupu ispisuje kao "41998" ili "45998", odnosno izvještava snijeg u krpama.
    U slučaju visine snijega manje od 0.5 cm, program će grupu šrifrirati kao "997".

    2.0.1 - Ispravljen kod za računanje 7. grupe u 3. odjeljku.
    U slučaju oborine u tragovima grupa program sada ispisuje grupu "79999", a u slučaju oborine 999,8 mm ili veće, program ispisuje "79998".

    2.0.0 - Promjenjen vizualni izgled aplikacije.
    Promjene u kodu aplikacije.
    Aplikacija ne prikazuje polje za unos temperature mora ako postaja ne izvještava temperaturu mora. U slučaju da postaja izvještava temperaturu mora, polje je vidljivo u svakom terminu, ali ako podatak nije unesen, u synop izvještaju je izostavljena grupa za temperaturu mora te cijeli drugi odjeljak.
    Polja za unos maksimalne temperature, minimalne temperature, minimalne temperature na 5 cm iznad tla te visine snijega i oborine su prikazani samo u terminima kada se ti podaci izvještavaju u synop izvještaju.
    Aplikacija izbacuje upozorenja ako nisu uneseni bitni podaci ili su podaci uneseni pogrešno.

    1.0.0 - Aplikacija računa stvarni tlak vodene pare, temperaturu rosišta i relativnu vlagu zraka te svodi tlak na 0°C i na SMR. 
    Također, aplikacija izračunava synop kodove te šifrira synop prvi odjeljak synop izvještaja, temperaturu mora u drugom odjeljku te 1., 2., 3. ,4. i 7. grupu trećeg odjeljka i u izvještaj uključuje i specijalne grupe koje motritelj sam unosi.
    Vizualni izgled aplikacije nije vezan za izgled dnevnika.-->
</html>
